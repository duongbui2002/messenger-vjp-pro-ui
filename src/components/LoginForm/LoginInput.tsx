import styles from "./LoginInput.module.scss"
import {UseFormRegister} from "react-hook-form";
import {FieldValue} from "react-hook-form";

interface LoginInputProps {
  placeholder:string
  register:UseFormRegister<FieldValue<any>>
}
export default function LoginInput({register}:LoginInputProps) {
  return (
    <div className={styles.container}>
      <input  {...register}/>
    </div>
  );
}