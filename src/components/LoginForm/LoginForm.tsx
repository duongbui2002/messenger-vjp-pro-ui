import styles from "./LoginForm.module.scss";
import {useForm} from "react-hook-form";
import {SubmitHandler} from "react-hook-form";
import * as yup from "yup";
import {yupResolver} from "@hookform/resolvers/yup";
import ErrorField from "@/components/shared/ErrorField/ErrorField.tsx";

interface ILoginFormInput {
  Email: string,
  Password: string
}

const loginSchema = yup.object({
  Email: yup.string().required(),
  Password: yup.string().required("Password is required").min(6, "Password must be at least 6 characters long"),
});

export default function LoginForm() {

  const {register, handleSubmit, formState: {errors}} = useForm<ILoginFormInput>({
      resolver: yupResolver(loginSchema)
    }
  );
  const onSubmit: SubmitHandler<ILoginFormInput> = data => {
    console.log(data);
  };
  return <div>
    <form onSubmit={handleSubmit(onSubmit)}>
      <div>

      </div>
      <div className={styles.container}>
        {errors.Email?.message && <ErrorField message={errors.Email.message}/>}
        <input placeholder="Email address or phone number"  {...register('Email', {required: true})}/>

      </div>
      <div className={styles.container}>
        {errors.Password?.message && <ErrorField message={errors.Password.message}/>}
        <input type="password" placeholder="Password"  {...register('Password', {
          required: true,
          minLength: 6,
          maxLength: 32
        })}/>

      </div>
      <div className={styles.submitContainer}>
        <button className={styles.loginButton} type="submit">Log In</button>
        <a href="#" className={styles.forgotPasswordLink}>Forgotten your password?</a>
      </div>
      <div>
        <div className={styles.checkBoxContainer}>
          <label>
            <input type="checkbox" value="1" className={styles.checkBox}/>
            <span className={styles.customCheckBox}></span>
          </label>
          <label className={styles.checkBoxExplain}>
            Keep me signed in
          </label>
        </div>
      </div>
    </form>
  </div>;
}