import styles from  "./IntroductionMain.module.scss"
import LoginForm from "@/components/LoginForm/LoginForm.tsx";
export default function IntroductionMain() {
  return <div className={styles.container}>
    <div className={styles.contentContainer}>
      <div>
        <div>
          <h1 className={styles.introText}>
            Hang out
            anytime, anywhere
          </h1>
        </div>

        <div>
          <p className={styles.advertiseText}>
            Messenger makes it easy and fun to stay close to your favourite people.
          </p>
        </div>

        <LoginForm/>

      </div>
      <div className={styles.rightContainer}>
        <div className={styles.introImgContainer}>
          <img src="/assets/advertise.png" alt="img-intro" className={styles.introImg}/>
        </div>
      </div>
    </div>
  </div>;
}