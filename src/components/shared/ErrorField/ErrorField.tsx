import styles from "./ErrorField.module.scss";

interface ErrorFieldProps {
  message: string;
}

export default function ErrorField({message}: ErrorFieldProps) {
  return (
    <p className={styles.error}>{message}</p>
  );
}