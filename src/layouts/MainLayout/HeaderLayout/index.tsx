import styles from "./Header.module.scss";
import Navigation from "@/layouts/MainLayout/HeaderLayout/Navigation.tsx";
import {useState} from "react";
import {useEffect} from "react";


export default function () {
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);
  return (
    <>

        {/*<div className={styles.container}>*/}
          <div className={styles.container}>
            <div className={styles.headerContainer}>
              <div className="">
                <img src="/assets/messenger-logo.png" alt="img" className={styles.logoImg}/>
              </div>
              <Navigation/>
            </div>
            <div className={`${styles.scrollBackground} ${scroll? styles.visibleOpacity : ""}`} ></div>
          </div>

        {/*</div>*/}



    </>
  );
}