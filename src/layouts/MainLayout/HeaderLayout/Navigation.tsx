import styles from './Navigation.module.scss';

export default function Navigation() {
  const navItems = ['Features', 'Desktop app', 'Privacy and safety', 'For developers'];
  return <ul className={styles.navList}>
    {navItems.map((ele, i) => <li className={styles.navItem} key={i}>
      <a href="#"> {ele}</a>
    </li>)}
  </ul>;


}