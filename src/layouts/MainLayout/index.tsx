import HeaderLayout from "@/layouts/MainLayout/HeaderLayout";

import IntroductionMain from "@/components/IntroductionMain/IntroductionMain.tsx";



export default function MainLayout() {
  return <>
    <HeaderLayout/>
    <IntroductionMain/>

  </>;
}